package comp5216.sydney.edu.au.moneyradar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import comp5216.sydney.edu.au.moneyradar.entities.AddFriendItem;

public class CreateRecordActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private DatabaseReference mDatabase;

    RadioGroup radioGroup;
    RadioButton rbLending;
    RadioButton rbBorrowing;
    EditText etTotalAmount;
    EditText etDeadline;
    EditText etDescription;
    ImageButton btnSelectDate;
    ImageButton btnSelectFriends;
    Button btnConfirm;
    DatePickerDialog dialog;
    ListView lvFriends;
    AddFriendAdapter friendsAdapter;
    ArrayList<AddFriendItem> friends;
    ArrayList<String> friendsUid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_record);
        setTitle("Create New Record");

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        radioGroup = (RadioGroup) findViewById(R.id.rg_lending_borrowing);
        rbLending = (RadioButton) findViewById(R.id.rb_lending);
        rbBorrowing = (RadioButton) findViewById(R.id.rb_borrowing);
        etTotalAmount = (EditText) findViewById(R.id.et_total);
        etDeadline = (EditText) findViewById(R.id.et_deadline);
        etDescription = (EditText) findViewById(R.id.et_description);
        btnSelectDate = (ImageButton) findViewById(R.id.button_select_date);
        btnSelectFriends = (ImageButton) findViewById(R.id.button_add_person);
        btnConfirm = (Button) findViewById(R.id.button_confirm);


        lvFriends = (ListView) findViewById(R.id.lv_friends);
        friends = new ArrayList<>();
        friendsUid = new ArrayList<>();
        friendsAdapter = new AddFriendAdapter(CreateRecordActivity.this, friends);
        lvFriends.setAdapter(friendsAdapter);

        lvFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                friends.remove(position);
                friendsUid.remove(position);
                friendsAdapter.notifyDataSetChanged();
            }
        });

        btnSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int month = cal.get(Calendar.MONTH);
                int year = cal.get(Calendar.YEAR);
                dialog = new DatePickerDialog(CreateRecordActivity.this, new dateSetListener(), year, month, day);
                dialog.show();
            }
        });
        btnSelectFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createViewFriendsDialog();

            }
        });
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openConfirmDialog();
            }
        });


    }


    private void openConfirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateRecordActivity.this);
        builder.setTitle("Confirm Record Creation");
        builder.setMessage("Are you sure you want to confirm creating this loan record?");
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //YES Cancel - go back to main page
                confirmRecordCreation();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //NO Cancel - close dialog with nothing else
            }
        });
        builder.create().show();
    }

    class dateSetListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            String date = Integer.toString(dayOfMonth) + "/" +
                    Integer.toString(month + 1) + "/" + Integer.toString(year);
            etDeadline.setText(date);
        }
    }

    private void confirmRecordCreation(){

        //Check if any field is null

        if(friends.size() < 1 ||
                etDescription.getText().toString() == "" ||
                etDeadline.getText().toString() == "" ||
                radioGroup.getCheckedRadioButtonId() == -1){
            Toast.makeText(this, "Please complete all fields", Toast.LENGTH_SHORT).show();
            return;
        }

        String lobUser;
        String lobFriend;
        //Lending or borrowing.
        if(rbLending.isChecked()){
            lobUser = "lent";
            lobFriend = "owed";
        }
        else{
            lobFriend = "lent";
            lobUser = "owed";
        }

        if(lobUser == "owed" && friends.size() != 1){
            Toast.makeText(this, "You can only include one friend when borrowing!", Toast.LENGTH_SHORT).show();
            return;
        }

        String resolved = "false";
        String description = etDescription.getText().toString();
        String deadline = etDeadline.getText().toString();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String date = sdf.format(new Date());

        double amountPerPerson = Double.parseDouble(etTotalAmount.getText().toString());
        amountPerPerson = amountPerPerson/friends.size();
        String amount = String.format("%.2f", amountPerPerson);

        for(AddFriendItem afi: friends){
            //Create records in database for friends AND current user.

            DatabaseReference userRef = mDatabase.child(mUser.getUid()).child("records").child(lobUser);
            DatabaseReference friendRef = mDatabase.child(afi.getUid()).child("records").child(lobFriend);

            String userPushKey = userRef.push().getKey();
            String friendPushKey = friendRef.push().getKey();

            userRef.child(userPushKey).child("amount").setValue(amount);
            userRef.child(userPushKey).child("date").setValue(date);
            userRef.child(userPushKey).child("deadline").setValue(deadline);
            userRef.child(userPushKey).child("description").setValue(description);
            userRef.child(userPushKey).child("resolved").setValue(resolved);
            userRef.child(userPushKey).child("friendId").setValue(afi.getUid());
            userRef.child(userPushKey).child("friendName").setValue(afi.getName());

            friendRef.child(userPushKey).child("amount").setValue(amount);
            friendRef.child(userPushKey).child("date").setValue(date);
            friendRef.child(userPushKey).child("deadline").setValue(deadline);
            friendRef.child(userPushKey).child("description").setValue(description);
            friendRef.child(userPushKey).child("resolved").setValue(resolved);
            friendRef.child(userPushKey).child("friendId").setValue(mUser.getUid());
            friendRef.child(userPushKey).child("friendName").setValue(mUser.getDisplayName());
        }

        Toast.makeText(CreateRecordActivity.this, "Loan Record Created", Toast.LENGTH_SHORT).show();
        finish();
    }

    private void createViewFriendsDialog() {

        mDatabase.child(mUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final ArrayList<AddFriendItem>  users = new ArrayList<>();
                Iterator<DataSnapshot> it = dataSnapshot.child("friends").getChildren().iterator();
                while(it.hasNext()){
                    DataSnapshot ds = it.next();
                    String uid = ds.getKey();
                    //Avoid viewing self
                    if(uid.equals(mUser.getUid())){
                        continue;
                    }
                    if(ds.child("name").getValue() != null){
                        String name = ds.child("name").getValue().toString();

                        AddFriendItem afi = new AddFriendItem(name, uid);
                        if(friendsUid.contains(afi.getUid())){

                        }else{
                            users.add(afi);
                        }
                    }
                }
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(CreateRecordActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View convertView = (View) inflater.inflate(R.layout.list, null);
                alertDialog.setView(convertView);
                alertDialog.setTitle("Select Friends");
                ListView lv = (ListView) convertView.findViewById(R.id.lv);
                final AddFriendAdapter adapter = new AddFriendAdapter(CreateRecordActivity.this, users);
                lv.setAdapter(adapter);

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, final int position, long rowId) {
                        Toast.makeText(CreateRecordActivity.this, users.get(position).getName() + " was added.", Toast.LENGTH_SHORT).show();
                        friends.add(users.get(position));
                        friendsUid.add(users.get(position).getUid());
                        friendsAdapter.notifyDataSetChanged();
                        users.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });

                alertDialog.show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

}

