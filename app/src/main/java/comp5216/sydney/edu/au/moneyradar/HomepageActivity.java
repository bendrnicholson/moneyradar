package comp5216.sydney.edu.au.moneyradar;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;

import comp5216.sydney.edu.au.moneyradar.entities.LoanRecord;
import comp5216.sydney.edu.au.moneyradar.util.LoanRecordAdapter;

//TODO: Report
//TODO: Presentation slides
//TODO: Readme/Manual

public class HomepageActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private DatabaseReference mDatabase;

    ListView listView;
    Button btnAllRecords;
    Button btnLentRecords;
    Button btnOwedRecords;
    ImageButton btnCreateNewRecord;
    ImageButton btnMessages;
    ImageButton btnAccount;

    TextView tvSortName;
    TextView tvSortDeadline;
    TextView tvSortAmount;

    String currentList;
    String currentSort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        setTitle("Homepage");

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //Set up views
        setupViews();

        //Initialize and display the "All Records" at the beginning, sorted by name.
        currentList = "all";
        currentSort = "name";
        initializeAllRecordsList();

        //Check for notifications
        checkNotifications();

    }


    @Override
    protected void onResume() {
        super.onResume();
        currentList = "all";
        currentSort = "name";
        initializeAllRecordsList();
    }

    private void checkNotifications() {

        DatabaseReference notificationsRef = mDatabase.child(mUser.getUid()).child("notifications");
        notificationsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterator<DataSnapshot> it = dataSnapshot.getChildren().iterator();
                while(it.hasNext()) {
                    final DataSnapshot ds = it.next();

                    if(ds.child("seen").getValue().toString().equals("false")){
                        //Display popup/dialog.

                        String message = "Remember that you owe " + ds.child("friendName").getValue().toString() +
                                        " $" + ds.child("amount").getValue().toString()+ " for  \"" +
                                        ds.child("description").getValue().toString() + "\", with the deadline being " +
                                        ds.child("deadline").getValue().toString() + "!";

                        AlertDialog.Builder builder = new AlertDialog.Builder(HomepageActivity.this);
                        builder.setTitle("You have a new notification");
                        builder.setMessage(message);
                        builder.setPositiveButton("Acknowledge", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mDatabase.child(mUser.getUid()).child("notifications").child(ds.getKey().toString()).child("seen").setValue("true");
                            }
                        });

                        builder.create().show();

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void initializeOwedRecordsList() {
        currentList = "owed";

        DatabaseReference owedRecordsRef = mDatabase.child(mUser.getUid()).child("records").child("owed");

        owedRecordsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final ArrayList<LoanRecord>  owedRecords = new ArrayList<>();
                Iterator<DataSnapshot> it = dataSnapshot.getChildren().iterator();
                while(it.hasNext()){
                    DataSnapshot ds = it.next();

                    LoanRecord lr = new LoanRecord();

                    String recordId = ds.getKey();
                    String amount = ds.child("amount").getValue().toString();
                    String date = ds.child("date").getValue().toString();
                    String deadline = ds.child("deadline").getValue().toString();
                    String description = ds.child("description").getValue().toString();
                    String friendId = ds.child("friendId").getValue().toString();
                    String friendName = ds.child("friendName").getValue().toString();
                    String resolved = ds.child("resolved").getValue().toString();
                    String userName = mUser.getDisplayName();
                    String userId = mUser.getUid();

                    lr.setId(recordId);
                    lr.setAmount(amount);
                    lr.setDateMade(date);
                    lr.setDeadlineDate(deadline);
                    lr.setDescription(description);
                    lr.setBorrowerId(userId);
                    lr.setBorrowerName(userName);
                    lr.setLenderId(friendId);
                    lr.setLenderName(friendName);
                    lr.setResolved(resolved);

                    if(lr.getResolved().equals("false")) {
                        owedRecords.add(lr);
                    }


                }

                if(currentSort.equals("name")){
                    Collections.sort(owedRecords, new LoanRecordLenderNameComparator());

                }
                else if(currentSort.equals("deadline")){
                    Collections.sort(owedRecords, new LoanRecordDeadlineComparator());

                }
                else if(currentSort.equals("amount")){
                    Collections.sort(owedRecords, new LoanRecordAmountComparator());

                }

                LoanRecordAdapter owedAdapter = new LoanRecordAdapter(HomepageActivity.this, owedRecords);
                listView.setAdapter(owedAdapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                        LoanRecord record = owedRecords.get(position);

                        Intent in = new Intent(HomepageActivity.this, YouOweActivity.class);
                        in.putExtra("record",(Parcelable) record);
                        startActivity(in);
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void initializeLentRecordsList() {

        currentList = "lent";

        DatabaseReference owedRecordsRef = mDatabase.child(mUser.getUid()).child("records").child("lent");

        owedRecordsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final ArrayList<LoanRecord>  lentRecords = new ArrayList<>();
                Iterator<DataSnapshot> it = dataSnapshot.getChildren().iterator();
                while(it.hasNext()){
                    DataSnapshot ds = it.next();

                    LoanRecord lr = new LoanRecord();

                    String recordId = ds.getKey();
                    String amount = ds.child("amount").getValue().toString();
                    String date = ds.child("date").getValue().toString();
                    String deadline = ds.child("deadline").getValue().toString();
                    String description = ds.child("description").getValue().toString();
                    String friendId = ds.child("friendId").getValue().toString();
                    String friendName = ds.child("friendName").getValue().toString();
                    String resolved = ds.child("resolved").getValue().toString();
                    String userName = mUser.getDisplayName();
                    String userId = mUser.getUid();

                    lr.setId(recordId);
                    lr.setAmount(amount);
                    lr.setDateMade(date);
                    lr.setDeadlineDate(deadline);
                    lr.setDescription(description);
                    lr.setBorrowerId(friendId);
                    lr.setBorrowerName(friendName);
                    lr.setLenderId(userId);
                    lr.setLenderName(userName);
                    lr.setResolved(resolved);

                    if(lr.getResolved().equals("false")) {
                        lentRecords.add(lr);
                    }


                }

                if(currentSort.equals("name")){
                    Collections.sort(lentRecords, new LoanRecordBorrowerNameComparator());

                }
                else if(currentSort.equals("deadline")){
                    Collections.sort(lentRecords, new LoanRecordDeadlineComparator());

                }
                else if(currentSort.equals("amount")){
                    Collections.sort(lentRecords, new LoanRecordAmountComparator());

                }

                LoanRecordAdapter lentAdapter = new LoanRecordAdapter(HomepageActivity.this, lentRecords);
                listView.setAdapter(lentAdapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                        LoanRecord record = lentRecords.get(position);

                        Intent in = new Intent(HomepageActivity.this, YouLentActivity.class);
                        in.putExtra("record",(Parcelable) record);
                        startActivity(in);
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void initializeAllRecordsList() {

        currentList= "all";

        DatabaseReference owedRecordsRef = mDatabase.child(mUser.getUid()).child("records");

        owedRecordsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final ArrayList<LoanRecord>  allRecords = new ArrayList<>();
                final ArrayList<LoanRecord>  owedRecords = new ArrayList<>();
                final ArrayList<LoanRecord>  lentRecords = new ArrayList<>();


                Iterator<DataSnapshot> it = dataSnapshot.child("lent").getChildren().iterator();
                while(it.hasNext()){
                    DataSnapshot ds = it.next();

                    LoanRecord lr = new LoanRecord();

                    String recordId = ds.getKey();
                    String amount = ds.child("amount").getValue().toString();
                    String date = ds.child("date").getValue().toString();
                    String deadline = ds.child("deadline").getValue().toString();
                    String description = ds.child("description").getValue().toString();
                    String friendId = ds.child("friendId").getValue().toString();
                    String friendName = ds.child("friendName").getValue().toString();
                    String resolved = ds.child("resolved").getValue().toString();
                    String userName = mUser.getDisplayName();
                    String userId = mUser.getUid();

                    lr.setId(recordId);
                    lr.setAmount(amount);
                    lr.setDateMade(date);
                    lr.setDeadlineDate(deadline);
                    lr.setDescription(description);
                    lr.setBorrowerId(friendId);
                    lr.setBorrowerName(friendName);
                    lr.setLenderId(userId);
                    lr.setLenderName(userName);
                    lr.setResolved(resolved);

                    if(lr.getResolved().equals("false")) {
                        lentRecords.add(lr);
                    }


                }


                Iterator<DataSnapshot> it2 = dataSnapshot.child("owed").getChildren().iterator();
                while(it2.hasNext()){
                    DataSnapshot ds = it2.next();

                    LoanRecord lr = new LoanRecord();

                    String recordId = ds.getKey();
                    String amount = ds.child("amount").getValue().toString();
                    String date = ds.child("date").getValue().toString();
                    String deadline = ds.child("deadline").getValue().toString();
                    String description = ds.child("description").getValue().toString();
                    String friendId = ds.child("friendId").getValue().toString();
                    String friendName = ds.child("friendName").getValue().toString();
                    String resolved = ds.child("resolved").getValue().toString();
                    String userName = mUser.getDisplayName();
                    String userId = mUser.getUid();

                    lr.setId(recordId);
                    lr.setAmount(amount);
                    lr.setDateMade(date);
                    lr.setDeadlineDate(deadline);
                    lr.setDescription(description);
                    lr.setBorrowerId(userId);
                    lr.setBorrowerName(userName);
                    lr.setLenderId(friendId);
                    lr.setLenderName(friendName);
                    lr.setResolved(resolved);

                    if(lr.getResolved().equals("false")) {
                        owedRecords.add(lr);
                    }

                }

                //Combine
                allRecords.addAll(owedRecords);
                allRecords.addAll(lentRecords);
                //Sort

                if(currentSort.equals("name")){
                    Collections.sort(allRecords, new LoanRecordAllNameComparator());

                }
                else if(currentSort.equals("deadline")){
                    Collections.sort(allRecords, new LoanRecordDeadlineComparator());

                }
                else if(currentSort.equals("amount")){
                    Collections.sort(allRecords, new LoanRecordAmountComparator());

                }


                LoanRecordAdapter allRecordsAdapter = new LoanRecordAdapter(HomepageActivity.this, allRecords);
                listView.setAdapter(allRecordsAdapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                        //Check whether it is owed or lent.

                        //User is the borrower.
                        if(allRecords.get(position).getBorrowerId().equals(mUser.getUid())){
                            LoanRecord record = allRecords.get(position);

                            Intent in = new Intent(HomepageActivity.this, YouOweActivity.class);
                            in.putExtra("record",(Parcelable) record);
                            startActivity(in);
                        }
                        //User is the lender.
                        else{
                            LoanRecord record = allRecords.get(position);

                            Intent in = new Intent(HomepageActivity.this, YouLentActivity.class);
                            in.putExtra("record",(Parcelable) record);
                            startActivity(in);
                        }


                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void setupViews() {
        listView = (ListView) findViewById(R.id.homepage_listview);
        btnAllRecords = (Button) findViewById(R.id.btn_all_records);
        btnLentRecords = (Button) findViewById(R.id.btn_money_lent);
        btnOwedRecords = (Button) findViewById(R.id.btn_money_owed);
        btnCreateNewRecord = (ImageButton) findViewById(R.id.btn_create_new_record);
        btnMessages = (ImageButton) findViewById(R.id.btn_messages);
        btnAccount = (ImageButton) findViewById(R.id.btn_account);

        tvSortName = (TextView) findViewById(R.id.tv_sort_name);
        tvSortDeadline = (TextView) findViewById(R.id.tv_sort_deadline);
        tvSortAmount = (TextView) findViewById(R.id.tv_sort_amount);

        btnAllRecords.setOnClickListener(this);
        btnLentRecords.setOnClickListener(this);
        btnOwedRecords.setOnClickListener(this);
        btnCreateNewRecord.setOnClickListener(this);
        btnMessages.setOnClickListener(this);
        btnAccount.setOnClickListener(this);

        tvSortName.setOnClickListener(this);
        tvSortDeadline.setOnClickListener(this);
        tvSortAmount.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        if(v.equals(btnAllRecords)){
            initializeAllRecordsList();
        }
        else if(v.equals(btnLentRecords)){
            initializeLentRecordsList();
        }
        else if(v.equals(btnOwedRecords)){
            initializeOwedRecordsList();
        }
        else if(v.equals(btnCreateNewRecord)){
            startActivity(new Intent(HomepageActivity.this, CreateRecordActivity.class));
        }
        else if(v.equals(btnMessages)){
            startActivity(new Intent(HomepageActivity.this, MessagesActivity.class));
        }

        else if(v.equals(btnAccount)){
            startActivity(new Intent(HomepageActivity.this, AccountActivity.class));
        }
        else if(v.equals(tvSortName)){
            currentSort = "name";

            if(currentList == "owed"){
                initializeOwedRecordsList();
            }
            else if(currentList == "lent"){
                initializeLentRecordsList();
            }
            else if(currentList == "all"){
                initializeAllRecordsList();
            }

        }
        else if(v.equals(tvSortDeadline)){
            currentSort = "deadline";
            if(currentList == "owed"){
                initializeOwedRecordsList();
            }
            else if(currentList == "lent"){
                initializeLentRecordsList();
            }
            else if(currentList == "all"){
                initializeAllRecordsList();
            }

        }
        else if(v.equals(tvSortAmount)){
            currentSort = "amount";
            if(currentList == "owed"){
                initializeOwedRecordsList();
            }
            else if(currentList == "lent"){
                initializeLentRecordsList();
            }
            else if(currentList == "all"){
                initializeAllRecordsList();
            }
        }

    }


}

class LoanRecordAllNameComparator implements Comparator<LoanRecord> {

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    public int compare(LoanRecord lr1, LoanRecord lr2) {

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        String name1;
        String name2;

        String userId = mUser.getUid();

        if(lr1.getBorrowerId().equals(userId)){
            name1 = lr1.getLenderName();
        }
        else{
            name1 = lr1.getBorrowerName();
        }

        if(lr2.getBorrowerId().equals(userId)){
            name2 = lr2.getLenderName();
        }
        else{
            name2 = lr2.getBorrowerName();
        }

        return name1.compareTo(name2);
    }
}

class LoanRecordLenderNameComparator implements Comparator<LoanRecord> {
    public int compare(LoanRecord lr1, LoanRecord lr2) {
        return lr1.getLenderName().compareTo(lr2.getLenderName());
    }
}

class LoanRecordBorrowerNameComparator implements Comparator<LoanRecord> {
    public int compare(LoanRecord lr1, LoanRecord lr2) {
        return lr1.getBorrowerName().compareTo(lr2.getBorrowerName());
    }
}

class LoanRecordAmountComparator implements Comparator<LoanRecord> {
    public int compare(LoanRecord lr1, LoanRecord lr2) {
        if(Double.parseDouble(lr1.getAmount()) < Double.parseDouble(lr2.getAmount())){
            return -1;
        }
        if(Double.parseDouble(lr1.getAmount()) > Double.parseDouble(lr2.getAmount())){
            return 1;
        }
        return 0;
    }
}

class LoanRecordDeadlineComparator implements Comparator<LoanRecord> {
    public int compare(LoanRecord lr1, LoanRecord lr2) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date d1 = sdf.parse(lr1.getDeadlineDate());
            Date d2 = sdf.parse(lr2.getDeadlineDate());

            Long l1 = d1.getTime();
            Long l2 = d2.getTime();

            return (int) (l1 - l2);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}