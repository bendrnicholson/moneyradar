package comp5216.sydney.edu.au.moneyradar;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

import comp5216.sydney.edu.au.moneyradar.entities.AddFriendItem;

public class MessagesActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private DatabaseReference mDatabase;

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        setTitle("Messages with friends");

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        listView = (ListView) findViewById(R.id.lv_messages);


        mDatabase.child(mUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final ArrayList<AddFriendItem> users = new ArrayList<>();
                Iterator<DataSnapshot> it = dataSnapshot.child("friends").getChildren().iterator();
                while(it.hasNext()){
                    DataSnapshot ds = it.next();
                    String uid = ds.getKey();
                    //Avoid viewing self
                    if(uid.equals(mUser.getUid())){
                        continue;
                    }
                    if(ds.child("name").getValue() != null){
                        String name = ds.child("name").getValue().toString();

                        AddFriendItem afi = new AddFriendItem(name, uid);
                        users.add(afi);
                    }
                }
                final AddFriendAdapter adapter = new AddFriendAdapter(MessagesActivity.this, users);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent in = new Intent(MessagesActivity.this, ConversationActivity.class);
                        in.putExtra("friend_id", users.get(position).getUid());
                        in.putExtra("friend_name", users.get(position).getName());
                        startActivity(in);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onClick(View v) {

    }
}
