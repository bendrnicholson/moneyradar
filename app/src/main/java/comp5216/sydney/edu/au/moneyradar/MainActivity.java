package comp5216.sydney.edu.au.moneyradar;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/*
This class just redirects upon startup, either to the sign in page, or to the homepage.
 */

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        //Redirect to signin - check auth first.
        if(mUser == null) {
            startActivity(new Intent(this, SignInActivity.class));
            finish();
        }else {
            startActivity(new Intent(this, HomepageActivity.class));
            finish();

//            TextView tvTemp = (TextView) findViewById(R.id.tv_tmp);
//            String userDetails =
//                    "Name: " + mUser.getDisplayName() +
//                            "\nEmail: " + mUser.getEmail() +
//                            "\nProvider Id: " + mUser.getProviderId() +
//                            "\nProviders: " + mUser.getProviders().toString() +
//                            "\nUID: " + mUser.getUid();
//
//            tvTemp.setText(userDetails);
        }
    }


    //Logout - temporary
    @Override
    public void onBackPressed() {
        mAuth.getInstance().signOut();
        Toast.makeText(MainActivity.this, "Signed out", Toast.LENGTH_LONG).show();
        startActivity(new Intent(MainActivity.this, SignInActivity.class));
        finish();
    }
}
