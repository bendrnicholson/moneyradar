package comp5216.sydney.edu.au.moneyradar.util;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.DecimalFormat;
import java.util.ArrayList;

import comp5216.sydney.edu.au.moneyradar.R;
import comp5216.sydney.edu.au.moneyradar.entities.LoanRecord;

//https://github.com/codepath/android_guides/wiki/Using-an-ArrayAdapter-with-ListView

public class LoanRecordAdapter extends ArrayAdapter<LoanRecord> {

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    public LoanRecordAdapter(Context context, ArrayList<LoanRecord> items){
        super(context, 0, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LoanRecord loanRecord = getItem(position);

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_loanrecord, parent, false);
        }

        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        TextView tvDeadline = (TextView) convertView.findViewById(R.id.tvDeadline);
        TextView tvAmount = (TextView) convertView.findViewById(R.id.tvAmount);
        TextView tvDescription = (TextView) convertView.findViewById(R.id.tv_description);

        DecimalFormat df2 = new DecimalFormat(".##");

        tvDeadline.setText(loanRecord.getDeadlineDate());
        tvDescription.setText("Description: " + loanRecord.getDescription());

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        if(mUser.getUid().equals(loanRecord.getBorrowerId())){
            //Current user is the borrower
            tvName.setText(loanRecord.getLenderName());
            String amount = "$" + df2.format(Double.parseDouble(loanRecord.getAmount()));
            tvAmount.setText(amount);
            tvAmount.setTextColor(Color.RED);
        }
        else{
            //Current user is lender
            tvName.setText(loanRecord.getBorrowerName());
            String amount = "$" + df2.format(Double.parseDouble(loanRecord.getAmount()));
            tvAmount.setText(amount);
            tvAmount.setTextColor(Color.GREEN);

        }
        return convertView;
    }
}
