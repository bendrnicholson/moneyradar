package comp5216.sydney.edu.au.moneyradar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import comp5216.sydney.edu.au.moneyradar.entities.LoanRecord;

public class YouOweActivity extends AppCompatActivity implements View.OnClickListener{

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private DatabaseReference mDatabase;

    Button btnSendMessage;

    TextView tvAmountOwed;
    TextView tvName;
    TextView tvDeadline;

    LoanRecord record;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_owe);
        setTitle("Owed Loan Record");

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        btnSendMessage = (Button) findViewById(R.id.button_send_message);
        btnSendMessage.setOnClickListener(this);
        tvAmountOwed = (TextView) findViewById(R.id.tv_amount_owed);
        tvName = (TextView) findViewById(R.id.tv_owed_person_name);
        tvDeadline = (TextView) findViewById(R.id.tv_deadline_date);

        Intent in = getIntent();
        record = (LoanRecord) in.getParcelableExtra("record");
//        Toast.makeText(this, record.toString(), Toast.LENGTH_SHORT).show();

        tvName.setText(record.getLenderName());
        tvAmountOwed.setText("$" + record.getAmount());
        tvDeadline.setText("Deadline: " + record.getDeadlineDate());


    }

    @Override
    public void onClick(View v) {
        if(v.equals(btnSendMessage)){
            //Open conversation activity with the other user
            Intent in = new Intent(YouOweActivity.this, ConversationActivity.class);
            in.putExtra("friend_id", record.getLenderId());
            in.putExtra("friend_name", record.getLenderName());
            startActivity(in);
        }

    }
}
