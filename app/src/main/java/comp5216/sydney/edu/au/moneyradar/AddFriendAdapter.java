package comp5216.sydney.edu.au.moneyradar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import comp5216.sydney.edu.au.moneyradar.entities.AddFriendItem;

public class AddFriendAdapter extends ArrayAdapter<AddFriendItem> {

    public AddFriendAdapter(Context context, ArrayList<AddFriendItem> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AddFriendItem addFriendItem = getItem(position);

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.add_friend_item, parent, false);
        }

        TextView tvItem = (TextView) convertView.findViewById(R.id.tvName);
//        ImageButton btnAdd= (ImageButton) convertView.findViewById(R.id.ib);

        tvItem.setText(addFriendItem.getName());

        return convertView;

    }
}