package comp5216.sydney.edu.au.moneyradar.entities;

/**
 * Created by Ben on 9/10/2016.
 */

public class AddFriendItem {

    private String name;
    private String uid;

    public AddFriendItem(String name, String uid) {
        this.name = name;
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "AddFriendItem{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                '}';
    }
}
