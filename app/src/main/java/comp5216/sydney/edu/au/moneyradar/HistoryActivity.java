package comp5216.sydney.edu.au.moneyradar;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import comp5216.sydney.edu.au.moneyradar.entities.LoanRecord;
import comp5216.sydney.edu.au.moneyradar.util.LoanRecordAdapter;


//TODO: (If have time) - instead of deadline, show date paid.

public class HistoryActivity extends AppCompatActivity implements View.OnClickListener{

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private DatabaseReference mDatabase;

    ListView listView;
    Button btnAllRecords;
    Button btnLentRecords;
    Button btnOwedRecords;

    TextView tvSortName;
    TextView tvSortDeadline;
    TextView tvSortAmount;

    String currentList;
    String currentSort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        setTitle("History (Resolved Loans)");

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //Set up views
        setupViews();

        //Initialize and display the "All Records" at the beginning, sorted by name.
        currentList = "all";
        currentSort = "name";
        initializeAllRecordsList();

    }

    private void initializeLentRecordsList() {

        currentList = "lent";

        DatabaseReference owedRecordsRef = mDatabase.child(mUser.getUid()).child("records").child("lent");

        owedRecordsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final ArrayList<LoanRecord>  lentRecords = new ArrayList<>();
                Iterator<DataSnapshot> it = dataSnapshot.getChildren().iterator();
                while(it.hasNext()){
                    DataSnapshot ds = it.next();

                    LoanRecord lr = new LoanRecord();

                    String recordId = ds.getKey();
                    String amount = ds.child("amount").getValue().toString();
                    String date = ds.child("date").getValue().toString();
                    String deadline = ds.child("deadline").getValue().toString();
                    String description = ds.child("description").getValue().toString();
                    String friendId = ds.child("friendId").getValue().toString();
                    String friendName = ds.child("friendName").getValue().toString();
                    String resolved = ds.child("resolved").getValue().toString();
                    String userName = mUser.getDisplayName();
                    String userId = mUser.getUid();

                    lr.setId(recordId);
                    lr.setAmount(amount);
                    lr.setDateMade(date);
                    lr.setDeadlineDate(deadline);
                    lr.setDescription(description);
                    lr.setBorrowerId(friendId);
                    lr.setBorrowerName(friendName);
                    lr.setLenderId(userId);
                    lr.setLenderName(userName);
                    lr.setResolved(resolved);

                    if(lr.getResolved().equals("true")) {
                        lentRecords.add(lr);
                    }
                }

                if(currentSort.equals("name")){
                    Collections.sort(lentRecords, new LoanRecordBorrowerNameComparator());

                }
                else if(currentSort.equals("deadline")){
                    Collections.sort(lentRecords, new LoanRecordDeadlineComparator());

                }
                else if(currentSort.equals("amount")){
                    Collections.sort(lentRecords, new LoanRecordAmountComparator());
                }

                LoanRecordAdapter lentAdapter = new LoanRecordAdapter(HistoryActivity.this, lentRecords);
                listView.setAdapter(lentAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void initializeOwedRecordsList() {
        currentList = "owed";

        DatabaseReference owedRecordsRef = mDatabase.child(mUser.getUid()).child("records").child("owed");

        owedRecordsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final ArrayList<LoanRecord> owedRecords = new ArrayList<>();
                Iterator<DataSnapshot> it = dataSnapshot.getChildren().iterator();
                while(it.hasNext()){
                    DataSnapshot ds = it.next();

                    LoanRecord lr = new LoanRecord();

                    String recordId = ds.getKey();
                    String amount = ds.child("amount").getValue().toString();
                    String date = ds.child("date").getValue().toString();
                    String deadline = ds.child("deadline").getValue().toString();
                    String description = ds.child("description").getValue().toString();
                    String friendId = ds.child("friendId").getValue().toString();
                    String friendName = ds.child("friendName").getValue().toString();
                    String resolved = ds.child("resolved").getValue().toString();
                    String userName = mUser.getDisplayName();
                    String userId = mUser.getUid();

                    lr.setId(recordId);
                    lr.setAmount(amount);
                    lr.setDateMade(date);
                    lr.setDeadlineDate(deadline);
                    lr.setDescription(description);
                    lr.setBorrowerId(userId);
                    lr.setBorrowerName(userName);
                    lr.setLenderId(friendId);
                    lr.setLenderName(friendName);
                    lr.setResolved(resolved);

                    if(lr.getResolved().equals("true")) {
                        owedRecords.add(lr);
                    }
                }
                if(currentSort.equals("name")){
                    Collections.sort(owedRecords, new LoanRecordLenderNameComparator());
                }
                else if(currentSort.equals("deadline")){
                    Collections.sort(owedRecords, new LoanRecordDeadlineComparator());
                }
                else if(currentSort.equals("amount")){
                    Collections.sort(owedRecords, new LoanRecordAmountComparator());
                }
                LoanRecordAdapter owedAdapter = new LoanRecordAdapter(HistoryActivity.this, owedRecords);
                listView.setAdapter(owedAdapter);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    private void initializeAllRecordsList() {

        currentList= "all";

        DatabaseReference owedRecordsRef = mDatabase.child(mUser.getUid()).child("records");

        owedRecordsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final ArrayList<LoanRecord>  allRecords = new ArrayList<>();
                final ArrayList<LoanRecord>  owedRecords = new ArrayList<>();
                final ArrayList<LoanRecord>  lentRecords = new ArrayList<>();

                Iterator<DataSnapshot> it = dataSnapshot.child("lent").getChildren().iterator();
                while(it.hasNext()){
                    DataSnapshot ds = it.next();

                    LoanRecord lr = new LoanRecord();

                    String recordId = ds.getKey();
                    String amount = ds.child("amount").getValue().toString();
                    String date = ds.child("date").getValue().toString();
                    String deadline = ds.child("deadline").getValue().toString();
                    String description = ds.child("description").getValue().toString();
                    String friendId = ds.child("friendId").getValue().toString();
                    String friendName = ds.child("friendName").getValue().toString();
                    String resolved = ds.child("resolved").getValue().toString();
                    String userName = mUser.getDisplayName();
                    String userId = mUser.getUid();

                    lr.setId(recordId);
                    lr.setAmount(amount);
                    lr.setDateMade(date);
                    lr.setDeadlineDate(deadline);
                    lr.setDescription(description);
                    lr.setBorrowerId(friendId);
                    lr.setBorrowerName(friendName);
                    lr.setLenderId(userId);
                    lr.setLenderName(userName);
                    lr.setResolved(resolved);

                    if(lr.getResolved().equals("true")) {
                        lentRecords.add(lr);
                    }
                }

                Iterator<DataSnapshot> it2 = dataSnapshot.child("owed").getChildren().iterator();
                while(it2.hasNext()){
                    DataSnapshot ds = it2.next();

                    LoanRecord lr = new LoanRecord();

                    String recordId = ds.getKey();
                    String amount = ds.child("amount").getValue().toString();
                    String date = ds.child("date").getValue().toString();
                    String deadline = ds.child("deadline").getValue().toString();
                    String description = ds.child("description").getValue().toString();
                    String friendId = ds.child("friendId").getValue().toString();
                    String friendName = ds.child("friendName").getValue().toString();
                    String resolved = ds.child("resolved").getValue().toString();
                    String userName = mUser.getDisplayName();
                    String userId = mUser.getUid();

                    lr.setId(recordId);
                    lr.setAmount(amount);
                    lr.setDateMade(date);
                    lr.setDeadlineDate(deadline);
                    lr.setDescription(description);
                    lr.setBorrowerId(userId);
                    lr.setBorrowerName(userName);
                    lr.setLenderId(friendId);
                    lr.setLenderName(friendName);
                    lr.setResolved(resolved);

                    if(lr.getResolved().equals("true")) {
                        owedRecords.add(lr);
                    }
                }
                //Combine
                allRecords.addAll(owedRecords);
                allRecords.addAll(lentRecords);
                //Sort
                if(currentSort.equals("name")){
                    Collections.sort(allRecords, new LoanRecordAllNameComparator());
                }
                else if(currentSort.equals("deadline")){
                    Collections.sort(allRecords, new LoanRecordDeadlineComparator());
                }
                else if(currentSort.equals("amount")){
                    Collections.sort(allRecords, new LoanRecordAmountComparator());
                }
                LoanRecordAdapter allRecordsAdapter = new LoanRecordAdapter(HistoryActivity.this, allRecords);
                listView.setAdapter(allRecordsAdapter);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void setupViews() {
        listView = (ListView) findViewById(R.id.homepage_listview);
        btnAllRecords = (Button) findViewById(R.id.btn_all_records);
        btnLentRecords = (Button) findViewById(R.id.btn_money_lent);
        btnOwedRecords = (Button) findViewById(R.id.btn_money_owed);

        tvSortName = (TextView) findViewById(R.id.tv_sort_name);
        tvSortDeadline = (TextView) findViewById(R.id.tv_sort_deadline);
        tvSortAmount = (TextView) findViewById(R.id.tv_sort_amount);

        btnAllRecords.setOnClickListener(this);
        btnLentRecords.setOnClickListener(this);
        btnOwedRecords.setOnClickListener(this);

        tvSortName.setOnClickListener(this);
        tvSortDeadline.setOnClickListener(this);
        tvSortAmount.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v.equals(btnAllRecords)){
            initializeAllRecordsList();
        }
        else if(v.equals(btnLentRecords)){
            initializeLentRecordsList();
        }
        else if(v.equals(btnOwedRecords)){
            initializeOwedRecordsList();
        }
        else if(v.equals(tvSortName)){
            currentSort = "name";

            if(currentList == "owed"){
                initializeOwedRecordsList();
            }
            else if(currentList == "lent"){
                initializeLentRecordsList();
            }
            else if(currentList == "all"){
                initializeAllRecordsList();
            }

        }
        else if(v.equals(tvSortDeadline)){
            currentSort = "deadline";
            if(currentList == "owed"){
                initializeOwedRecordsList();
            }
            else if(currentList == "lent"){
                initializeLentRecordsList();
            }
            else if(currentList == "all"){
                initializeAllRecordsList();
            }

        }
        else if(v.equals(tvSortAmount)){
            currentSort = "amount";
            if(currentList == "owed"){
                initializeOwedRecordsList();
            }
            else if(currentList == "lent"){
                initializeLentRecordsList();
            }
            else if(currentList == "all"){
                initializeAllRecordsList();
            }
        }

    }


}
