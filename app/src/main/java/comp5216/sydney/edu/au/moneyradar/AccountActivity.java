package comp5216.sydney.edu.au.moneyradar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

import comp5216.sydney.edu.au.moneyradar.entities.AddFriendItem;

public class AccountActivity extends AppCompatActivity implements View.OnClickListener{

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private DatabaseReference mDatabase;

    EditText etName;
    Button btnAddFriends;
    Button btnViewFriends;
    Button btnSave;
    Button btnLogout;
    Button btnHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        setTitle("My Account");

        etName = (EditText) findViewById(R.id.et_name);
        btnAddFriends = (Button) findViewById(R.id.btn_add_friends);
        btnViewFriends = (Button) findViewById(R.id.btn_view_friends);
        btnSave = (Button) findViewById(R.id.btn_save_account);
        btnLogout = (Button) findViewById(R.id.btn_logout);
        btnHistory = (Button) findViewById(R.id.btn_view_history);

        btnAddFriends.setOnClickListener(this);
        btnViewFriends.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
        btnHistory.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();


        //Set the editText

        final String userID = mUser.getUid();
        //Path is: user_id>account>name
        mDatabase = FirebaseDatabase.getInstance().getReference();

//        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.d("SignInActivity", "onDataChange called...");
//
//                Object name = dataSnapshot.child(userID).child("account").child("name").getValue();
//
//                if(name == null){
//                    etName.setText(mUser.getDisplayName());
//
//                }
//                else{
//                    etName.setText(name.toString());
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                Log.d("SignInActivity", databaseError.toString());
//
//            }
//        });

        etName.setText(mUser.getDisplayName());


    }

    @Override
    public void onClick(View view) {
        if(view.equals(btnSave)){
            if(etName.getText().toString().length() != 0) {
                mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.child(mUser.getUid()).child("account").child("name").setValue(etName.getText().toString());

                startActivity(new Intent(AccountActivity.this, HomepageActivity.class));
                finish();
            }
            else{
                Toast.makeText(this, "Please enter a name.", Toast.LENGTH_SHORT).show();
            }
        }
        else if(view.equals(btnAddFriends)){
            //Open a dialog that shows all users, with a button that can add a user as a friend.
            //Saves the friend to path: userID>friends>friendID>name>friendName

            createAddFriendsDialog();


        }
        else if(view.equals(btnViewFriends)){

            createViewFriendsDialog();
        }
        else if(view.equals(btnLogout)){
            mAuth.getInstance().signOut();
            Toast.makeText(AccountActivity.this, "Signed out", Toast.LENGTH_LONG).show();
            startActivity(new Intent(AccountActivity.this, SignInActivity.class));
            finish();
        }
        else if(view.equals(btnHistory)){
            startActivity(new Intent(AccountActivity.this, HistoryActivity.class));
        }

    }

    private void createViewFriendsDialog() {

        mDatabase.child(mUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final ArrayList<AddFriendItem>  users = new ArrayList<>();
                Iterator<DataSnapshot> it = dataSnapshot.child("friends").getChildren().iterator();
                while(it.hasNext()){
                    DataSnapshot ds = it.next();
                    String uid = ds.getKey();
                    //Avoid viewing self
                    if(uid.equals(mUser.getUid())){
                        continue;
                    }
                    if(ds.child("name").getValue() != null){
                        String name = ds.child("name").getValue().toString();

                        AddFriendItem afi = new AddFriendItem(name, uid);
                        users.add(afi);
                    }
                }
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(AccountActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View convertView = (View) inflater.inflate(R.layout.list, null);
                alertDialog.setView(convertView);
                alertDialog.setTitle("Your Friends");
                ListView lv = (ListView) convertView.findViewById(R.id.lv);
                final AddFriendAdapter adapter = new AddFriendAdapter(AccountActivity.this, users);
                lv.setAdapter(adapter);
                alertDialog.show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void createAddFriendsDialog() {

        //Read all users into an array and display.
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final ArrayList<AddFriendItem>  users = new ArrayList<>();
                ArrayList<String> userIds = new ArrayList<>();
                Iterator<DataSnapshot> it = dataSnapshot.child(mUser.getUid()).child("friends").getChildren().iterator();
                while(it.hasNext()){
                    DataSnapshot ds = it.next();
                    String uid = ds.getKey();
                    //Avoid viewing self
                    if(uid.equals(mUser.getUid())){
                        continue;
                    }
                    if(ds.child("name").getValue() != null){
                        String name = ds.child("name").getValue().toString();

                        AddFriendItem afi = new AddFriendItem(name, uid);
                        users.add(afi);
                        userIds.add(uid);
                        Log.d("Users 1", afi.toString());
                    }
                }

                final ArrayList<AddFriendItem>  users2 = new ArrayList<>();
                Iterator<DataSnapshot> it2 = dataSnapshot.getChildren().iterator();
                while(it2.hasNext()){
                    DataSnapshot ds = it2.next();
                    String uid = ds.getKey();
                    //Avoid viewing self
                    if(uid.equals(mUser.getUid())){
                        continue;
                    }
                    if(ds.child("account").child("name").getValue() != null){
                        String name = ds.child("account").child("name").getValue().toString();
                        AddFriendItem afi = new AddFriendItem(name, uid);

                        //Check to display only people that are not the current user's friends.
                        if(!userIds.contains(uid)){
                            users2.add(afi);
                            Log.d("AddingFriends", uid + ", " + name);
                        }

                    }
                }

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(AccountActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View convertView = (View) inflater.inflate(R.layout.list, null);
                alertDialog.setView(convertView);
                alertDialog.setTitle("Add Friends");
                ListView lv = (ListView) convertView.findViewById(R.id.lv);
                final AddFriendAdapter adapter = new AddFriendAdapter(AccountActivity.this, users2);
                lv.setAdapter(adapter);

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, final int position, long rowId) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(AccountActivity.this);
                        builder.setTitle("Add Friend");
                        builder.setMessage("Do you want to add " + users2.get(position).getName() + " as a friend?");
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Add user as friend.

                                mDatabase.child(mUser.getUid()).child("friends").child(users2.get(position).getUid()).child("name").setValue(users2.get(position).getName());


                                Toast.makeText(AccountActivity.this, "Added friend: " + users2.get(position).getName() + ", " + users2.get(position).getUid(), Toast.LENGTH_SHORT).show();
                                users2.remove(position);
                                adapter.notifyDataSetChanged();

                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        builder.create().show();

                    }
                });


                alertDialog.show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

}
