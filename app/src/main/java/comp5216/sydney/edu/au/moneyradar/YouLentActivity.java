package comp5216.sydney.edu.au.moneyradar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import comp5216.sydney.edu.au.moneyradar.entities.LoanRecord;

public class YouLentActivity extends AppCompatActivity implements View.OnClickListener{

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private DatabaseReference mDatabase;

    Button btnResolve;
    Button btnSendReminder;
    Button btnSendMessage;

    TextView tvAmountOwed;
    TextView tvName;
    TextView tvDeadline;

    LoanRecord record;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_lent);
        setTitle("Lent Loan Record");

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        btnResolve = (Button) findViewById(R.id.button_resolve);
        btnSendReminder = (Button) findViewById(R.id.button_send_reminder_notification);
        btnSendMessage = (Button) findViewById(R.id.button_send_message);

        tvAmountOwed = (TextView) findViewById(R.id.tv_amount_owed);
        tvName = (TextView) findViewById(R.id.tv_owed_person_name);
        tvDeadline = (TextView) findViewById(R.id.tv_deadline_date);



        btnResolve.setOnClickListener(this);
        btnSendReminder.setOnClickListener(this);
        btnSendMessage.setOnClickListener(this);

        Intent in = getIntent();
        record = (LoanRecord) in.getParcelableExtra("record");
//        Toast.makeText(this, record.toString(), Toast.LENGTH_SHORT).show();

        //set the name, amount owed, and deadline
        tvName.setText(record.getBorrowerName());
        tvAmountOwed.setText("$" + record.getAmount());
        tvDeadline.setText("Deadline: " + record.getDeadlineDate());

    }


    @Override
    public void onClick(View v) {
        if(v.equals(btnResolve)){
            openResolveButtonDialog();
        }
        else if (v.equals(btnSendReminder)){
            openSendReminderDialog();
        }
        else if(v.equals(btnSendMessage)){
            //Open conversation activity with the other user
            Intent in = new Intent(YouLentActivity.this, ConversationActivity.class);
            in.putExtra("friend_id", record.getBorrowerId());
            in.putExtra("friend_name", record.getBorrowerName());
            startActivity(in);
        }
    }

    private void openSendReminderDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(YouLentActivity.this);
        builder.setTitle("Confirm Send Notification");
        builder.setMessage("Are you sure you want to send a notification to remind " + record.getBorrowerName() + " to pay you back for this record?");
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                confirmNotificationSend();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.create().show();
    }

    private void confirmNotificationSend(){

        //Send notification to the borrower account.
        mDatabase.child(record.getBorrowerId()).child("notifications").child(record.getLenderId()).child("amount").setValue(record.getAmount());
        mDatabase.child(record.getBorrowerId()).child("notifications").child(record.getLenderId()).child("deadline").setValue(record.getDeadlineDate());
        mDatabase.child(record.getBorrowerId()).child("notifications").child(record.getLenderId()).child("friendName").setValue(record.getLenderName());
        mDatabase.child(record.getBorrowerId()).child("notifications").child(record.getLenderId()).child("description").setValue(record.getDescription());
        mDatabase.child(record.getBorrowerId()).child("notifications").child(record.getLenderId()).child("seen").setValue("false");

        Toast.makeText(this, "Notification has been sent!", Toast.LENGTH_SHORT).show();
    }

    private void openResolveButtonDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(YouLentActivity.this);
        builder.setTitle("Confirm Resolving Record");
        builder.setMessage("Are you sure you want to resolve this loan record?");
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                confirmRecordResolution();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.create().show();

    }

    private void confirmRecordResolution(){
        //Change this loan record resolved field to true.

        mDatabase.child(record.getLenderId()).child("records").child("lent").child(record.getId()).child("resolved").setValue("true");
        mDatabase.child(record.getBorrowerId()).child("records").child("owed").child(record.getId()).child("resolved").setValue("true");

        Toast.makeText(this, "Loan record resolved!", Toast.LENGTH_SHORT).show();
        finish();
    }

}
