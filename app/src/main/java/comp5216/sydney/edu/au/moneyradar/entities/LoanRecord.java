package comp5216.sydney.edu.au.moneyradar.entities;

//To be used for ListView adapter


import android.os.Parcel;
import android.os.Parcelable;

public class LoanRecord implements Parcelable {

    String id;
    String lenderName;
    String borrowerName;
    String lenderId;
    String borrowerId;
    String description;
    String amount;
    String dateMade;
    String deadlineDate;
    String resolved;

    public LoanRecord(){}

    public LoanRecord(String lenderName, String borrowerName, String lenderId, String borrowerId, String description, String amount, String dateMade, String deadlineDate, String resolved) {
        this.lenderName = lenderName;
        this.borrowerName = borrowerName;
        this.lenderId = lenderId;
        this.borrowerId = borrowerId;
        this.description = description;
        this.amount = amount;
        this.dateMade = dateMade;
        this.deadlineDate = deadlineDate;
        this.resolved = resolved;
    }

    protected LoanRecord(Parcel in) {
        id = in.readString();
        lenderName = in.readString();
        borrowerName = in.readString();
        lenderId = in.readString();
        borrowerId = in.readString();
        description = in.readString();
        amount = in.readString();
        dateMade = in.readString();
        deadlineDate = in.readString();
        resolved = in.readString();
    }

    public static final Creator<LoanRecord> CREATOR = new Creator<LoanRecord>() {
        @Override
        public LoanRecord createFromParcel(Parcel in) {
            return new LoanRecord(in);
        }

        @Override
        public LoanRecord[] newArray(int size) {
            return new LoanRecord[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(lenderName);
        parcel.writeString(borrowerName);
        parcel.writeString(lenderId);
        parcel.writeString(borrowerId);
        parcel.writeString(description);
        parcel.writeString(amount);
        parcel.writeString(dateMade);
        parcel.writeString(deadlineDate);
        parcel.writeString(resolved);
    }

    @Override
    public String toString() {
        return "LoanRecord{" +
                "id='" + id + '\'' +
                ", lenderName='" + lenderName + '\'' +
                ", borrowerName='" + borrowerName + '\'' +
                ", lenderId='" + lenderId + '\'' +
                ", borrowerId='" + borrowerId + '\'' +
                ", description='" + description + '\'' +
                ", amount='" + amount + '\'' +
                ", dateMade='" + dateMade + '\'' +
                ", deadlineDate='" + deadlineDate + '\'' +
                ", resolved='" + resolved + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLenderName() {
        return lenderName;
    }

    public void setLenderName(String lenderName) {
        this.lenderName = lenderName;
    }

    public String getBorrowerName() {
        return borrowerName;
    }

    public void setBorrowerName(String borrowerName) {
        this.borrowerName = borrowerName;
    }

    public String getLenderId() {
        return lenderId;
    }

    public void setLenderId(String lenderId) {
        this.lenderId = lenderId;
    }

    public String getBorrowerId() {
        return borrowerId;
    }

    public void setBorrowerId(String borrowerId) {
        this.borrowerId = borrowerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDateMade() {
        return dateMade;
    }

    public void setDateMade(String dateMade) {
        this.dateMade = dateMade;
    }

    public String getDeadlineDate() {
        return deadlineDate;
    }

    public void setDeadlineDate(String deadlineDate) {
        this.deadlineDate = deadlineDate;
    }

    public String getResolved() {
        return resolved;
    }

    public void setResolved(String resolved) {
        this.resolved = resolved;
    }


}
